package de.awacademy.model;

public class Rechteck {

    // Eigenschaften
    private int x;
    private int y;
    private int h;
    private int w;
    private float speedY;
    private int number;
    private boolean yellow = false;

    // Konstruktoren
    public Rechteck(int x, int y, float speedY, int number) {
        this.x = x;
        this.y = y;
        this.h = 60;
        this.w = 90;
        this.speedY = speedY;
        this.number = number;
    }

    // Methoden
    public void update(long elapsedTime){
        this.y = Math.round(this.y + elapsedTime*speedY);
    }

    public void increaseSpeed(){
        this.speedY = getSpeedY() + 0.01f;
    }


    // Getter und Setter
    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }

    public float getSpeedY() {
        return speedY;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void setYellow(boolean isYellow){
        this.yellow = true;
    }

    public void setNotYellow(boolean isYellow){
        this.yellow = false;
    }

    public boolean isYellow() {
        return yellow;
    }

    public void toStart(){
        this.y = 60;
    }

}
