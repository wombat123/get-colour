package de.awacademy.model;

import java.util.ArrayList;
import java.util.List;

public class Model {

    // FINALS
    public static final int WIDTH = 600;
    public static final int HEIGHT = 600;


    // Eigenschaften
    private List<Rechteck> zahlen = new ArrayList<>();
    private Integer round = 1;


    // Konstruktoren
    public Model() {
        Rechteck linkeBox = new Rechteck(250, 60, 0.1f, 1);
        Rechteck rechteBox = new Rechteck(350, 60, 0.1f, 2);

        this.zahlen.add(linkeBox);
        this.zahlen.add(rechteBox);
    }

    // Methoden
    public void update (long elapsedTime) {
        for (Rechteck n : zahlen){
            n.update(elapsedTime);
        }
    }

    // Setter + Getter
    public List<Rechteck> getZahlen() {
        return zahlen;
    }

    public Integer getRound() {
        return round;
    }
    public Integer increaseRound(){
        this.round++;
        return round;
    }

}
