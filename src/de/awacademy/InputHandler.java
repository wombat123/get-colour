package de.awacademy;

import de.awacademy.model.Model;
import javafx.scene.input.KeyCode;

public class InputHandler {

    // Eigenschaften
    private Model model;
    private static Dir direction = Dir.up;

    // Konstruktoren
    public InputHandler (Model model) {
        this.model = model;
    }

    // Methoden
    public enum Dir {
        left, right, up, down
    }

    public void onKeyPressed(KeyCode key) {

        if (key == KeyCode.W) {
            direction = Dir.up;
        }
        else if(key == KeyCode.A) {
            direction = Dir.left;
        }
        else if(key == KeyCode.D) {
            direction = Dir.right;
        }

    }

    // Getter und Setter
    public static Dir getDirection() {
        return direction;
    }

    public static void setDirection(Dir direction) {
        InputHandler.direction = direction;
    }



}
