package de.awacademy;

import de.awacademy.model.Model;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


public class MainGame extends Application {

    // Eigenschaften initialisieren
    private Timer timer;


    // Methoden
    @Override
    public void start(Stage stage) {

        try {
            VBox root = new VBox();

            // Canvas
            Canvas canvas = new Canvas(Model.WIDTH, Model.HEIGHT);

            // Draw
            GraphicsContext gc = canvas.getGraphicsContext2D();
            root.getChildren().add(canvas);

            //Scene
            Scene scene = new Scene(root, Model.WIDTH, Model.HEIGHT);

            // Stage
            stage.setTitle("Get Colour");
            stage.setScene(scene);
            stage.show();

            Model model = new Model();
            Graphics graphics = new Graphics(model,gc);


            timer = new Timer(model, graphics);
            timer.start();

            // InputHandler
            InputHandler inputHandler = new InputHandler(model);

            scene.setOnKeyPressed(
                    event -> inputHandler.onKeyPressed(event.getCode())
            );



        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void stop() throws Exception {
        timer.stop();
        super.stop();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
