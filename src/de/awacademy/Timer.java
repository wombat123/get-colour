package de.awacademy;


import de.awacademy.model.Model;
import javafx.animation.AnimationTimer;

import java.util.Random;

public class Timer extends AnimationTimer {

    // Eigenschaften
    private long previousTime = -1;
    private Model model;
    private Graphics graphics;


    // Kontruktoren
    public Timer(Model model, Graphics graphics) {
        this.model = model;
        this.graphics = graphics;
    }

    // Methoden

    @Override
    public void handle(long nowNano) {

        long nowMilli = nowNano/1000000;
        long elapsedTime;
        if (previousTime == -1) {
            elapsedTime = 0;
        } else {
            elapsedTime = nowMilli - previousTime;
        }


        previousTime = nowMilli;
        if (graphics.isGameOver()){
            graphics.gameOver();
            return;
        } else {
            model.update(elapsedTime);
            graphics.draw();
            switch (InputHandler.getDirection()) {
                case left:
                    if (model.getZahlen().get(0).isYellow()) {
                        model.increaseRound();
                        model.getZahlen().get(0).toStart();
                        model.getZahlen().get(1).toStart();
                        model.getZahlen().get(0).increaseSpeed();
                        model.getZahlen().get(1).increaseSpeed();
                        InputHandler.setDirection(InputHandler.Dir.up);
                        Random r = new Random();
                        int result = r.nextInt(2);
                        model.getZahlen().get(0).setNumber(result);
                        model.getZahlen().get(1).setNumber(result+1);

                    } else {
                        graphics.setGameOver(true);
                    }
                    break;
                case right:
                    if (model.getZahlen().get(1).isYellow()) {
                        model.increaseRound();
                        model.getZahlen().get(0).toStart();
                        model.getZahlen().get(1).toStart();
                        model.getZahlen().get(0).increaseSpeed();
                        model.getZahlen().get(1).increaseSpeed();
                        InputHandler.setDirection(InputHandler.Dir.up);
                        Random r = new Random();
                        int result = r.nextInt(2);
                        model.getZahlen().get(0).setNumber(result);
                        model.getZahlen().get(1).setNumber(result+1);
                    } else {
                        graphics.setGameOver(true);
                    }
            }


        }
    }


}
