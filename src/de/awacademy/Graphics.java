package de.awacademy;

import de.awacademy.model.Model;
import de.awacademy.model.Rechteck;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;


public class Graphics {

    // Eigenschaften
    private Model model;
    private GraphicsContext gc;
    private boolean gameOver = false;

    // Konstruktoren
    public Graphics(Model model, GraphicsContext gc) {
        this.model = model;
        this.gc = gc;
    }

    // Getter und Setter
    public boolean isGameOver() {
        return gameOver;
    }

    public void setGameOver(boolean gameOver) {
        this.gameOver = gameOver;
    }

    // Methoden

    public void gameOver(){
        gc.setFill(Color.RED);
        gc.setFont(Font.font("Arial Rounded MT Bold", FontWeight.BOLD, 40));
        gc.fillText("\tGAME OVER", Model.HEIGHT/6, Model.WIDTH/4);
    }

    public void draw() {

        // Clear Screen
        gc.clearRect(0,0, Model.WIDTH, Model.HEIGHT);

        // Draw Background
        gc.setFill(Color.WHITE);
        gc.fillRect(0, 0, Model.WIDTH, Model.HEIGHT);
        gc.setFill(Color.DIMGRAY);
        gc.fillRect(0, 60, Model.WIDTH/4, Model.HEIGHT);
        gc.setFill(Color.DIMGRAY);
        gc.fillRect(450, 60, Model.WIDTH/4, Model.HEIGHT);


        // Draw Rectangles
        for (Rechteck n : this.model.getZahlen()) {

            System.out.println(model.getZahlen().get(0).getNumber());
            if (n.getNumber()%2 == 0) {
                gc.setFill(Color.DARKGRAY);
                gc.fillRect(
                        n.getX() - n.getW() / 2,
                        n.getY() - n.getH() / 2,
                        n.getW(),
                        n.getH()
                );
                n.setYellow(true);
            } else{
                gc.setFill(Color.YELLOW);
                gc.fillRect(
                        n.getX() - n.getW() / 2,
                        n.getY() - n.getH() / 2,
                        n.getW(),
                        n.getH()
                );
                n.setNotYellow(false);
            }


            gc.setFill(Color.BLACK);
            gc.setFont(Font.font("Arial Rounded MT Bold", FontWeight.BOLD, 30));
            gc.fillText("SCORE: " + model.getRound(), 10, 50);



            if (n.getY() + n.getH()/2 >= Model.HEIGHT){
                gameOver = true;
                gameOver();
            }


        }



    }

}
